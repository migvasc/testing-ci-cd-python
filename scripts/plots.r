library(gridExtra)
library(tidyverse)
library(patchwork)
library(viridis)
library(stringr)

args<-commandArgs(TRUE)

if (length(args)==0) {
  stop("At least one argument must be supplied (input file).n", call.=FALSE)
} 


year <- args[1]
year <- str_replace(year, ".json", "")


###################################### FIGURE 1 ###################################################

generate_figure_1_partial <- function(file,title)
 {
   #df <- read_delim(skip=25,file, col_names = c("date",'utc_time',"temperature", "relative_humidity", "pressure","wind_speed","wind_direction","rainfall","snowfall","snow_depth","ghi"), delim = ";", show_col_types = FALSE)
   
  df <- read_delim(skip=1,file, col_names = c("timestamp","ghi"), delim = ";", show_col_types = FALSE)
  df$date <- as.POSIXct(df$timestamp, format="%Y-%m-%d", tz="UTC")
  avg_df <- aggregate(df[, 2], list(df$date), mean) # column 11 is the GHI
  avg_df$ut_date <- as.POSIXct(avg_df$Group.1, format="%Y-%m-%d", tz="UTC")

  plot <- ggplot(data=avg_df, aes(x=ut_date,y=ghi)) +
    geom_area(colour="#5ec962",fill="#5ec962") +
    scale_x_datetime(labels = date_format("%Y-%m-%d %H"),
                    date_breaks = "1 month",date_labels =  "%b")+
    theme_bw() +     
    labs(x="Time in months",
        y="Mean Daily GHI (Wh/m\u00B2)")+

    theme(legend.text = element_text(size = 16),legend.title =element_blank(),
          axis.text.x = element_text(size = 10,angle=90),axis.text.y = element_text(size = 10),
          axis.title.x= element_text(size = 18),axis.title.y= element_text(size = 18),
          plot.title = element_text(size = 16,face="bold",hjust = 0.5))+
    ggtitle(title)  
    return(plot)
 }
 
 solar_input <- paste("input/solar_irradiation_data/year/johannesburg/", year, '.csv',sep='')
 plot1 <-generate_figure_1_partial(solar_input, 'Johanesbourg')

 solar_input <- paste("input/solar_irradiation_data/year/pune/", year, '.csv',sep='')
 plot2 <-generate_figure_1_partial(solar_input, 'Pune')

 solar_input <- paste("input/solar_irradiation_data/year/canberra/", year, '.csv',sep='')
 plot3 <-generate_figure_1_partial(solar_input, 'Canberra')

 solar_input <- paste("input/solar_irradiation_data/year/dubai/", year, '.csv',sep='')
 plot4 <-generate_figure_1_partial(solar_input, 'Dubai')

 solar_input <- paste("input/solar_irradiation_data/year/singapore/", year, '.csv',sep='')
 plot5 <-generate_figure_1_partial(solar_input, 'Singapore')

 solar_input <- paste("input/solar_irradiation_data/year/seoul/", year, '.csv',sep='')
 plot6 <-generate_figure_1_partial(solar_input, 'Seoul')

 solar_input <- paste("input/solar_irradiation_data/year/virginia/", year, '.csv',sep='')
 plot7 <-generate_figure_1_partial(solar_input, 'Virginia')

 solar_input <- paste("input/solar_irradiation_data/year/sp/", year, '.csv',sep='')
 plot8 <-generate_figure_1_partial(solar_input, 'S\u00E3o Paulo')

 solar_input <- paste("input/solar_irradiation_data/year/paris/", year, '.csv',sep='')
 plot9 <-generate_figure_1_partial(solar_input, 'Paris')
 page <-grid.arrange(plot1, plot2 , plot3 , plot4,plot5, plot6, plot7, plot8, plot9,
                     nrow = 3)

 output  <- paste("results/", year, '/figure_1.pdf',sep='')

 ggsave(
   output,
   plot = page,
   device = 'pdf',
   path = './',
   scale = 1,
   width = 10,
   height = 10,
   dpi = 300,
   units = c("in"),
   limitsize = FALSE
 )
 
######################################### FIGURE 2 #####################################################

file  <- paste("results/", year, '/figure_2_data.csv',sep='')
df <- read_delim(file,  delim = ";", show_col_types = FALSE)
df_bat = filter(df, type == "battery")
df_pv = filter(df, type == "pv")

max_first  <- max(df_bat$value)   # Specify max of first y axis
max_second <- max(df_pv$value) # Specify max of second y axis
min_first  <- min(df_bat$value)   # Specify min of first y axis
min_second <- min(df_pv$value) # Specify min of second y axis
# scale and shift variables calculated based on desired mins and maxes
scale = (max_second - min_second)/(max_first - min_first)
shift = min_first - min_second


df_scale <- df %>% mutate(scaled_value=ifelse(type=="battery",value,value/scale))
my_order <- as.character(c('DC_JOHANNESBURG','DC_PUNE','DC_CANBERRA','DC_DUBAI','DC_SINGAPORE','DC_SEOUL', 'DC_VIRGINIA', 'DC_SP', 'DC_PARIS'))    # the `as.character` calls are only
final_plot <- ggplot(df_scale,aes(x=dc, y = scaled_value,fill= type)) + 
  theme_bw()+
  geom_col(position="dodge") + 
  scale_fill_viridis_d(name ="Type:",labels = c("Battery", "Photovoltaic Panel"))  +
  scale_y_continuous(sec.axis = sec_axis(~ . *scale, name = "PV Area (m\u00B2)"))+
  scale_x_discrete(limits = function(x) my_order[my_order %in% x])+
  labs(x= "Data center", y="Battery Capacity (kWh)")+
  
  theme(legend.position="bottom",,legend.text = element_text(size = 19),legend.title = element_text(size = 19),
        axis.text.x = element_text(angle = 90,size = 18),axis.text.y = element_text(size = 14),
        axis.title.x= element_text(size = 20), axis.title.y= element_text(size = 20),
        plot.title = element_text(size = 16,face="bold",hjust = 0.5))

output_file <- paste("results/", year, '/figure_2.pdf',sep='')
ggsave(
  output_file,
  plot = final_plot,
  device = 'pdf',
  path = './',
  scale = 1,
  width = 10,
  height = 6,
  dpi = 300,
  units = c("in"),
  limitsize = FALSE
)

########################################## FIGURE 3 #####################################################

generate_figure_3_partial <- function(file,dc_name,title, legend){
  df <- read_delim(file,  delim = ";", show_col_types = FALSE)
  df_dc = filter(df, dc == dc_name)
  plot <- ggplot(df_dc,aes(x=day, y = value,fill= type)) + 
    theme_bw()+
    geom_area() + 
    ggtitle( title)+
    scale_fill_viridis_d(name ="Energy source:",labels = c("Discharged from battery", "Photovoltaic", "Electrical grid"))    +
    labs(x= "Day", y="Energy Ratio")


  if (legend) {
    plot <- plot +  theme(legend.position="bottom", legend.box = "vertical",legend.text = element_text(size = 19),legend.title = element_text(size = 19),
          axis.text.x = element_text(angle = 90,size = 18),axis.text.y = element_text(size = 14),
          axis.title.x= element_text(size = 20), axis.title.y= element_text(size = 20),
          plot.title = element_text(size = 16,face="bold",hjust = 0.5))

  }
  else {

    plot <- plot + theme(legend.position="none", 
        axis.text.x = element_text(angle = 90,size = 18),axis.text.y = element_text(size = 14),
        axis.title.x= element_text(size = 20), axis.title.y= element_text(size = 20),
        plot.title = element_text(size = 16,face="bold",hjust = 0.5))

  }
  
  return(plot)
}

file <- paste("results/", year, '/figure_3_data.csv',sep='')


plot1 <- generate_figure_3_partial(file,'DC_JOHANNESBURG','Johannesburg',FALSE)
plot2 <- generate_figure_3_partial(file,'DC_PUNE', 'Pune',FALSE)
plot3 <- generate_figure_3_partial(file,'DC_CANBERRA', 'Canberra',FALSE)
plot4 <- generate_figure_3_partial(file,'DC_DUBAI','Dubai',FALSE)
plot5 <- generate_figure_3_partial(file,'DC_SINGAPORE', 'Singapore',FALSE)
plot6 <- generate_figure_3_partial(file,'DC_SEOUL','Seoul',FALSE)
plot7 <- generate_figure_3_partial(file,'DC_VIRGINIA','Virginia',FALSE)
plot8 <- generate_figure_3_partial(file,'DC_SP', 'S\u00E3o Paulo',FALSE)
plot9 <- generate_figure_3_partial(file,'DC_PARIS','Paris',TRUE)


final_plot <- plot1 / plot2 / plot3 / plot4 / plot5/ plot6/ plot7/ plot8/ plot9

output_file <- paste("results/", year, '/figure_3.pdf',sep='')

ggsave(
  output_file,
  plot = final_plot,
  device = 'pdf',
  path  = './',
  scale = 1,
  width = 10,
  height = 21,
  dpi = 300,
  units = c("in"),
  limitsize = FALSE
)



############################################ FIGURE 4 #####################################################

 
 get_max_value_1_week <- function(file) {
   df <- read_delim(file,  delim = ";", show_col_types = FALSE)
   max_pv = max(df$pv, na.rm = TRUE)
   max_grid = max(df$grid, na.rm = TRUE)
   max_bat = max(df$bat, na.rm = TRUE)
   max_pow = max(df$power, na.rm = TRUE)
   maxs = c(max_pv,max_bat,max_pow,max_grid)
   max_y = max(maxs,na.rm = TRUE)   
   return (max_y)
 }

 generate_figure_4_partial <- function(file, dc_name, i, j, title, legend, max_y) {
            
   df <- read_delim(file,  delim = ";", show_col_types = FALSE)
   df_dc = filter(df, dc == dc_name)

   df_pv = df_dc[c("time","pv")]
   df_pv$type <- 'Photovoltaic'
   names(df_pv)[2] <- 'value'  
   df_grid = df_dc[c("time","grid")]
   df_grid$type <- 'Electrical grid'
   names(df_grid)[2] <- 'value'      
   df_bat = df_dc[c("time","bat")]
   df_bat$type <- 'Discharged from battery'
   names(df_bat)[2] <- 'value'   
   df_power = df_dc[c("time","power")]
   df_power$type <- 'DC Power demand'
   names(df_power)[2] <- 'value'
   new_df <- bind_rows(df_pv, df_grid, df_power, df_bat)

   plot <- ggplot(new_df,aes(x=time, y = value,color= type,linetype =type, shape = type)) + 
     theme_bw()+
     geom_line() + 
     geom_point() +
     xlim(i, j)+     
     ylim(0,max_y)+
     labs(x= "Time (hour)", y="Power (W)")+
     ggtitle(title)+
     scale_color_manual(values = c("#31688e", "#440154", "#fde725","#21918c"))+
     labs(color  = "Source", linetype = "Source", shape = "Source")

  if (legend) {
    plot <- plot +  theme(legend.position="bottom", legend.box = "vertical",legend.text = element_text(size = 19),legend.title = element_text(size = 19),
           axis.text.x = element_text(angle = 90,size = 18),axis.text.y = element_text(size = 14),
           axis.title.x= element_text(size = 20), axis.title.y= element_text(size = 20),
           plot.title = element_text(size = 16,face="bold",hjust = 0.5))
  }
  else {
    plot <- plot + theme(legend.position="none", 
           axis.text.x = element_text(,size = 18),axis.text.y = element_text(size = 14),
           axis.title.x= element_text(size = 20), axis.title.y= element_text(size = 20),
           plot.title = element_text(size = 16,face="bold",hjust = 0.5))
  }
   
   return(plot)
 }


file  <- paste("results/", year, '/figure_4_data.csv',sep='')
max_y <- get_max_value_1_week(file)
plot1 <- generate_figure_4_partial(file,'DC_JOHANNESBURG',1,24,'Johannesburg',FALSE,max_y)
plot2 <- generate_figure_4_partial(file,'DC_PUNE',1,24,'Pune',FALSE,max_y)
plot3 <- generate_figure_4_partial(file,'DC_CANBERRA',1,24,'Canberra',FALSE,max_y)
plot4 <- generate_figure_4_partial(file,'DC_DUBAI',1,24,'Dubai',FALSE,max_y)
plot5 <- generate_figure_4_partial(file,'DC_SINGAPORE',1,24,'Singapore',FALSE,max_y)
plot6 <- generate_figure_4_partial(file,'DC_SEOUL',1,24,'Seoul',FALSE,max_y)
plot7 <- generate_figure_4_partial(file,'DC_VIRGINIA',1,24,'Virginia',FALSE,max_y)
plot8 <- generate_figure_4_partial(file,'DC_SP',1,24,'S\u00E3o Paulo',FALSE,max_y)
plot9 <- generate_figure_4_partial(file,'DC_PARIS',1,24,'Paris',TRUE,max_y)
final_plot <- plot1 / plot2 / plot3 / plot4 / plot5/ plot6/ plot7/ plot8/ plot9
output  <- paste("results/", year, '/figure_4.pdf',sep='')

ggsave(
  output,
  plot = final_plot,
  device = 'pdf',
  path = './',
  scale = 1,
  width = 12,
  height = 23,
  dpi = 300,
  units = c("in"),
  limitsize = FALSE
)
