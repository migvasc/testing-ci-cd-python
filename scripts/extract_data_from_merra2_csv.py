def extract_irradiance_from_merra2_csv_file(merra_2_csv_path,new_file_path):    
        
    result_file = open(new_file_path, 'w')    
    result_file.write('timestamp;solar_irradiation\n')

    with open(merra_2_csv_path, encoding="UTF-8") as irradiation_file:    
        cursor = 0
        for line_number, line in enumerate(irradiation_file):
            if line_number <= 24: # First 24 lines are description of the input
                continue
            row = line.split(';')
            if(len(row)<10): # The csv has 10 columns, this condition is only valid at the end of the file
                break                
            timestamp = row[0] +" "+  row[1] 
            solar_irradiation = float(row[10].replace('\n',''))
            result_file.write(f'{timestamp};{solar_irradiation}\n')                
    result_file.close()
    
files = [
    "input/solar_irradiation_data/year/canberra/2018_merra2.csv", 
    "input/solar_irradiation_data/year/seoul/2018_merra2.csv",
    "input/solar_irradiation_data/year/paris/2018_merra2.csv",
    "input/solar_irradiation_data/year/virginia/2018_merra2.csv",
    "input/solar_irradiation_data/year/dubai/2018_merra2.csv",
    "input/solar_irradiation_data/year/singapore/2018_merra2.csv", 
    "input/solar_irradiation_data/year/pune/2018_merra2.csv", 
    "input/solar_irradiation_data/year/johannesburg/2018_merra2.csv", 
    "input/solar_irradiation_data/year/sp/2018_merra2.csv",
    "input/solar_irradiation_data/year/canberra/2019_merra2.csv", 
    "input/solar_irradiation_data/year/seoul/2019_merra2.csv",
    "input/solar_irradiation_data/year/paris/2019_merra2.csv",
    "input/solar_irradiation_data/year/virginia/2019_merra2.csv",
    "input/solar_irradiation_data/year/dubai/2019_merra2.csv",
    "input/solar_irradiation_data/year/singapore/2019_merra2.csv", 
    "input/solar_irradiation_data/year/pune/2019_merra2.csv", 
    "input/solar_irradiation_data/year/johannesburg/2019_merra2.csv", 
    "input/solar_irradiation_data/year/sp/2019_merra2.csv",
    "input/solar_irradiation_data/year/canberra/2020_merra2.csv", 
    "input/solar_irradiation_data/year/seoul/2020_merra2.csv",
    "input/solar_irradiation_data/year/paris/2020_merra2.csv",
    "input/solar_irradiation_data/year/virginia/2020_merra2.csv",
    "input/solar_irradiation_data/year/dubai/2020_merra2.csv",
    "input/solar_irradiation_data/year/singapore/2020_merra2.csv", 
    "input/solar_irradiation_data/year/pune/2020_merra2.csv", 
    "input/solar_irradiation_data/year/johannesburg/2020_merra2.csv", 
    "input/solar_irradiation_data/year/sp/2020_merra2.csv", 
    "input/solar_irradiation_data/year/canberra/2021_merra2.csv", 
    "input/solar_irradiation_data/year/seoul/2021_merra2.csv",
    "input/solar_irradiation_data/year/paris/2021_merra2.csv",
    "input/solar_irradiation_data/year/virginia/2021_merra2.csv",
    "input/solar_irradiation_data/year/dubai/2021_merra2.csv",
    "input/solar_irradiation_data/year/singapore/2021_merra2.csv", 
    "input/solar_irradiation_data/year/pune/2021_merra2.csv", 
    "input/solar_irradiation_data/year/johannesburg/2021_merra2.csv", 
    "input/solar_irradiation_data/year/sp/2021_merra2.csv"
]


for path in files:
    new_path = path.replace('_merra2', '') 
    extract_irradiance_from_merra2_csv_file(path,new_path)