#!/bin/bash
mkdir -p results

## Running the experiments
python3 scripts/low_carbon_cloud.py --input_file "input/$1" 

## Extract the data
python3 scripts/extract_data_figures.py --input_file "input/$1"

## Generate the plots
Rscript scripts/custom_plots.r $1 
