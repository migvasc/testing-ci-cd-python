library(withr)
library(tidyr)
library(dplyr)
library(Dict)
library(tidyverse)
library(ggplot2)
library(reshape2)
library(viridis)
library(gtable)
library(grid)

library(colourvalues)
library(viridis)
library(stringr)
library(gridExtra)
require(scales)
#install.packages("ggthemes")
library(patchwork)
#install.packages("patchwork")
library(magrittr)
library(tidyverse)
library(ggplot2)
library(dplyr)

# ADDS A TERMINATOR TO A MAP TO SHOW DAYTIME / NIGHTTIME REGIONS
# Returns a dataframe of latitude and longitude for the line that separates illuminated day and dark night for any given time
# This is just a port of the Javascript Leaflet.Terminator plugin (https://github.com/joergdietrich/Leaflet.Terminator/blob/master/L.Terminator.js)

rad2deg <- function(rad) {
  (rad * 180) / (pi)
}

deg2rad <- function(deg) {
  (deg * pi) / (180)
}

getJulian <- function(time) {
  # get Julian day (number of days since noon on January 1, 4713 BC; 2440587.5 is number of days between Julian epoch and UNIX epoch)
  (as.integer(time) / 86400) + 2440587.5
}

getGMST <- function(jDay) {
  # calculate Greenwich Mean Sidereal Time
  d <- jDay - 2451545.0
  (18.697374558 + 24.06570982441908 * d) %% 24
}

sunEclipticPosition <- function(jDay) {
  # compute the position of the Sun in ecliptic coordinates
  # days since start of J2000.0
  n <- jDay - 2451545.0
  # mean longitude of the Sun
  L <- 280.460 + 0.9856474 * n
  L = L %% 360
  # mean anomaly of the Sun
  g <- 357.528 + 0.9856003 * n
  g = g %% 360
  # ecliptic longitude of Sun
  lambda <- L + 1.915 * sin(deg2rad(g)) + 0.02 * sin(2 * deg2rad(g))
  # distance from Sun in AU
  R <- 1.00014 - 0.01671 * cos(deg2rad(g)) - 0.0014 * cos(2 * deg2rad(g))
  
  data.frame(lambda, R)
}

eclipticObliquity <- function(jDay) {
  # compute ecliptic obliquity
  n <- jDay - 2451545.0
  # Julian centuries since J2000.0
  T <- n / 36525
  # compute epsilon
  23.43929111 -
    T * (46.836769 / 3600
         - T * (0.0001831 / 3600
                + T * (0.00200340 / 3600
                       - T * (0.576e-6 / 3600
                              - T * 4.34e-8 / 3600))))
}

sunEquatorialPosition <- function(sunEclLng, eclObliq) {
  # compute the Sun's equatorial position from its ecliptic position
  alpha <- rad2deg(atan(cos(deg2rad(eclObliq)) *
                          tan(deg2rad(sunEclLng))))
  delta <- rad2deg(asin(sin(deg2rad(eclObliq)) 
                        * sin(deg2rad(sunEclLng))))
  
  lQuadrant  = floor(sunEclLng / 90) * 90
  raQuadrant = floor(alpha / 90) * 90
  alpha = alpha + (lQuadrant - raQuadrant)
  
  data.frame(alpha, delta)
}

hourAngle <- function(lng, sunPos, gst) {
  # compute the hour angle of the sun for a longitude on Earth
  lst <- gst + lng / 15
  lst * 15 - sunPos$alpha
}

longitude <- function(ha, sunPos) {
  # for a given hour angle and sun position, compute the latitude of the terminator
  rad2deg(atan(-cos(deg2rad(ha)) / tan(deg2rad(sunPos$delta))))
}

terminator <- function(time, from = -180, to = 180, by = 0.1) {
  # calculate latitude and longitude of terminator within specified range using time (in POSIXct format, e.g. `Sys.time()`)
  jDay = getJulian(time)
#  print("jDay")
#  print(jDay)
  gst = getGMST(jDay)
#  print("gst")
 # print(gst)
  
  sunEclPos = sunEclipticPosition(jDay)
 # print("sunEclPos")
#  print(sunEclPos)
  
  eclObliq = eclipticObliquity(jDay)
 # print("eclObliq")
 # print(eclObliq)
  
  sunEqPos = sunEquatorialPosition(sunEclPos$lambda, eclObliq)
  #print("sunEqPos")
#  print(sunEqPos)
  
  lapply(seq(from, to, by), function(i) {
    ha = hourAngle(i, sunEqPos, gst)
    lon = longitude(ha, sunEqPos)
    data.frame(lat = i, lon)
  }) %>%
    plyr::rbind.fill()
}





# # EXAMPLES
# # terminator for current time on world map
#
# # add terminator at current time to world map as shaded region using `geom_ribbon``

# 
# # add terminator at a specific time to map of Europe, using a `coord_*` function to crop after drawing shaded region with `geom_ribbon`
#ggplot2::ggplot() +
   #borders("world", colour = "gray90", fill = "gray85") +
  # geom_ribbon(data = terminator(as.POSIXct("2018-01-01 07:00:00 GMT"), -180, 190, 0.1), aes(lat, ymax = lon), ymin = 90, alpha = 0.2) +
 #  coord_equal(xlim = c(35, -12), ylim = c(35, 72), expand = 0) +
#   ggthemes::theme_map()


   #install.packages("Dict")
 
 
   
   
   
 dcs_lat <- Dict$new(
   "DC_CANBERRA"=-35.29759, 
   "DC_SEOUL"=37.56668, 
   "DC_PARIS"=48.85889, 
   "DC_VIRGINIA"=37.12322,
   "DC_DUBAI"=25.26535,
   "DC_SINGAPORE"=1.35711, 
   "DC_PUNE"=18.52143, 
   "DC_JOHANNESBURG" = -26.20500, 
   "DC_SP"=  -23.48620,
   .overwrite = TRUE
 )
 
 dcs_long <- Dict$new(
   "DC_CANBERRA" = 149.10127, 
   "DC_SEOUL" = 126.97829, 
   "DC_PARIS" = 2.32004, 
   "DC_VIRGINIA" =-78.492,
   "DC_DUBAI" =55.29249,
   "DC_SINGAPORE" =103.8195, 
   "DC_PUNE" = 73.85445, 
   "DC_JOHANNESBURG" = 28.04972, 
   "DC_SP" =  -46.50092,
   .overwrite = TRUE
 )


 timeslot = 8000
 
 path = '/home/migvasc/plots/'
 file <- '/home/migvasc/Desktop/OLD LAPTOP/PHD/greencloud/src/results_gurobi/results/2021/solution.sol'
 df <- read_delim(file, delim = ';', col_names = c("dc", "value"))
 
   timeslots <- 0:24
 
 
   for(timeslot in timeslots){
     date_value <-  as.POSIXct("2022-01-01 00:00:00 UT")  
     date_value <- date_value + lubridate::hours(timeslot)
     date_value_title <-  as.POSIXct("2021-01-01 00:00:00 UT")  
     date_value_title <- date_value_title + lubridate::hours(timeslot)
     
     plot <- ggplot() +
       borders("world", colour = "gray90", fill = "gray85") +
       geom_ribbon(data = terminator(date_value, -180, 190), aes(lat, ymax = lon), ymin = 90, alpha = 0.2) +
       coord_equal() +
       ggthemes::theme_map() 
     
     #  "DC_CANBERRA"
     dc = "DC_CANBERRA"
     key <-  paste("w__",dc,"_",as.character(timeslot),sep = "")
     values = filter(df, dc == key)
     value = (as.numeric(values$value)/278400)*20
     plot <- plot + geom_point(aes(x=dcs_long["DC_CANBERRA"],y=dcs_lat["DC_CANBERRA"]),size=value)
     
     
     dc = "DC_PARIS"
     key <-  paste("w__",dc,"_",as.character(timeslot),sep = "")
     values = filter(df, dc == key)
     value = (as.numeric(values$value)/278400)*20
     plot <- plot + geom_point(aes(x=dcs_long["DC_PARIS"],y=dcs_lat["DC_PARIS"]),size=value)
     
     
     
     dc = "DC_SEOUL"
     key <-  paste("w__",dc,"_",as.character(timeslot),sep = "")
     values = filter(df, dc == key)
     value = (as.numeric(values$value)/278400)*20
     plot <- plot + geom_point(aes(x=dcs_long["DC_SEOUL"],y=dcs_lat["DC_SEOUL"]),size=value)
     
     dc = "DC_VIRGINIA"
     key <-  paste("w__",dc,"_",as.character(timeslot),sep = "")
     values = filter(df, dc == key)
     value = (as.numeric(values$value)/278400)*20
     plot <- plot + geom_point(aes(x=dcs_long["DC_VIRGINIA"],y=dcs_lat["DC_VIRGINIA"]),size=value)
     
     
     dc = "DC_DUBAI"
     key <-  paste("w__",dc,"_",as.character(timeslot),sep = "")
     values = filter(df, dc == key)
     value = (as.numeric(values$value)/278400)*20
     plot <- plot + geom_point(aes(x=dcs_long["DC_DUBAI"],y=dcs_lat["DC_DUBAI"]),size=value)
     
     
     dc = "DC_SINGAPORE"
     key <-  paste("w__",dc,"_",as.character(timeslot),sep = "")
     values = filter(df, dc == key)
     value = (as.numeric(values$value)/278400)*20
     plot <- plot + geom_point(aes(x=dcs_long["DC_SINGAPORE"],y=dcs_lat["DC_SINGAPORE"]),size=value)
     
     
     dc = "DC_PUNE"
     key <-  paste("w__",dc,"_",as.character(timeslot),sep = "")
     values = filter(df, dc == key)
     value = (as.numeric(values$value)/278400)*20
     plot <- plot + geom_point(aes(x=dcs_long["DC_PUNE"],y=dcs_lat["DC_PUNE"]),size=value)
     
     dc = "DC_JOHANNESBURG"
     key <-  paste("w__",dc,"_",as.character(timeslot),sep = "")
     values = filter(df, dc == key)
     value = (as.numeric(values$value)/278400)*20
     plot <- plot + geom_point(aes(x=dcs_long["DC_JOHANNESBURG"],y=dcs_lat["DC_JOHANNESBURG"]),size=value)
     
     
     dc = "DC_SP"
     key <-  paste("w__",dc,"_",as.character(timeslot),sep = "")
     values = filter(df, dc == key)
     value = (as.numeric(values$value)/278400)*20
     plot <- plot + geom_point(aes(x=dcs_long["DC_SP"],y=dcs_lat["DC_SP"]),size=value)
     
     title <- paste("DC Load for instant ",date_value_title)
     plot <-plot+ theme(
       panel.background = element_rect(fill = "white",
                                       colour = "white"),
       plot.background = element_rect(fill = "white"),
       plot.title= element_text(size = 20,face="bold",hjust = 0.5))+
       ggtitle( title)
     
     
     number_name <- 
       with_options(
         c(scipen = 999), 
         str_pad(timeslot, 8, pad = "0")
       )
     
     full_file_name = paste(number_name,".png", sep="")
     ggsave(
       full_file_name,
       plot = plot,
       device = 'png',
       path = path,
       scale = 1,
       width = 14,
       height = 5,
       dpi = 300,
       units = c("in"),
       limitsize = FALSE
     )
     print(timeslot)
   }
   
   
   if(!require("tidyverse")) install.packages("tidyverse")
   if(!require("sf")) install.packages("sf")
   if(!require("lubridate")) install.packages("lubridate")
   
   t0 <-as.POSIXct("2021-03-20 10:00:00 UT")
   coord_nightday <- terminator(t0, -180, 180, 0.2) # estimate the day-night line
   
   
  
   
   
   
   
   plot <- ggplot() +
     borders("world", colour = "gray90", fill = "gray85") +
     geom_ribbon(data = terminator(as.POSIXct("2023-03-20 11:00:00 UT")  , -180, 190,1), aes(lat, ymax = lon), ymin = 90, alpha = 0.2) +
     coord_equal() +
     ggthemes::theme_map() 
   plot
   