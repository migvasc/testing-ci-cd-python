from pulp import *
import time
import argparse
import os
from util import Util

#### Util functions

class LowCarbonCloudLP:

    def validate_inputs(self,name, timeslots, DCs, workload, irradiance, C, pv_co2, grid_co2, pIdleDC, pNetIntraDC, PUE, pCore, bat_co2, eta_ch, eta_dch, eta_pv):
        if pCore == '' or pCore is None:
            raise ValueError('pCore cannot be empty!')
    
    def __init__(self):#, name, timeslots, DCs, workload, irradiance, C, pv_co2, grid_co2, pIdleDC, pNetIntraDC, PUE, pCore, bat_co2, eta_ch, eta_dch, eta_pv):
        parser = argparse.ArgumentParser(description='Low-carbon cloud sizing tool')
        parser.add_argument('--input_file', metavar = 'input_file', type=str,
                            help = 'filename with the inputs')
        parser.add_argument('--use_gurobi', help = 'use the gurobi solver', action='store_true')
        parser.add_argument('--only_write_lp_file', help = 'Write the .lp file to be used with an external solver', action='store_true')
        parser.add_argument('--write_sol_file', help = 'Write the variables and their values to a .csv file', action='store_true')
        res = parser.parse_args()  
        
        self.args = res
        use_gurobi = res.use_gurobi
        only_write_lp_file = res.only_write_lp_file
        input_file = res.input_file

        if input_file is None or input_file == '':
          raise RuntimeError('Need to pass a input file!')


        

    ### Auxiliary functions
    def getDCPowerConsumption(self,d,k):
        if(k ==0):
            return 0
        return self.PUE[d] * (self.pNetIntraDC[d]+ self.pIdleDC[d]  + self.w[d][k] * self.pCore ) * 1/1000
    
    def FPgrid(self,d,k):
        return self.grid_co2[d] * self.Pgrid[d][k]

    def FPpv(self,d,k):
        return self.Pre(d,k) * self.pv_co2[d]

    def FPbat(self,d):
        return self.Bat[d] * self.bat_co2

    def P(self,d,k):
        return self.getDCPowerConsumption(d,k) 

    def Pre(self,d,k):
        return (self.A[d] * self.irradiance[d][k] * self.eta_pv) / 1000 # convert to kw

    def use_original_objective_function(self,prob):    
        prob +=  lpSum(
            [  self.FPgrid(d,k)  + self.FPpv(d,k)  for k in self.timeslots ]  + self.FPbat(d) for d in self.DCs)  
        return prob        
        
    def use_Pdch_obj_function(self,prob):    
        prob +=  lpSum(
            [  self.FPgrid(d,k)  + self.FPpv(d,k)  + self.Pdch[d][k]*self.pv_co2[d]*0.0000000001 for k in self.timeslots ]  + self.FPbat(d) for d in self.DCs)  
        return prob

    
    def build_lp(self):        
        prob = LpProblem(self.name, LpMinimize)

        for d in self.DCs:
            prob.addConstraint( self.Pch[d][0]   == 0.0 )
            prob.addConstraint( self.Pdch[d][0]   == 0.0 )

        for d in self.DCs:
            for k in self.timeslots[1:] :
                prob.addConstraint( self.B[d][k]  == self.B[d][k-1] + self.Pch[d][k]*self.eta_ch  - self.Pdch[d][k]*self.eta_dch )
                prob.addConstraint( self.Pch[d][k]  * self.eta_ch <= 0.8 * self.Bat[d] - self.B[d][k-1] )   # To ensure that only charge/discharge if
                prob.addConstraint( self.Pdch[d][k] * self.eta_dch <= self.B[d][k-1] -  0.2 * self.Bat[d] ) # battery capacity >= 0 


        for d in self.DCs:
            for k in self.timeslots :
                prob.addConstraint( self.P(d,k) <= self.Pgrid[d][k] + self.Pre(d,k) + self.Pdch[d][k] - self.Pch[d][k]  )
                prob.addConstraint( self.FPgrid(d,k) >= 0 )
                prob.addConstraint( self.Pre(d,k) >= self.Pch[d][k]   )
                prob.addConstraint( self.B[d][k]   >= 0.2 * self.Bat[d] )
                prob.addConstraint( self.B[d][k]   <= 0.8 * self.Bat[d] )
                prob.addConstraint( self.w[d][k]<=self.C[d])

        for k in self.timeslots:
                prob +=  lpSum([  self.w[d][k]   for d in self.DCs]) == self.workload[k]    
        
        return prob




def write_sol_file(filename,dict_variables):
    if not os.path.exists('results/'+filename):
        os.mkdir('results/'+filename)
    result_file = open(f'results/{filename}/solution.csv', 'w')    
    for d in dict_variables:                        
        result_file.write(f'{d};{round(dict_variables[d],2)}\n')                                                          
    result_file.close()

def write_summary(total_emissions,runtime,input_file,output_path):
    result_file = open(output_path, 'w')    
    result_file.write('input_file;total_emissions;runtime\n')
    result_file.write(f'{input_file};{total_emissions};{runtime}\n')
    result_file.close()


def main():
  LowCarbonCloudLP()

if __name__ == '__main__':
    main()
