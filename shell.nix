 { pkgs ? import <nixpkgs> { } }:
 with pkgs;
 let

  R-with-my-packages = rWrapper.override{ packages = with rPackages; [ tidyverse gridExtra patchwork viridis stringr ]; };
  my-python = pkgs.python3;
  python-with-my-packages = my-python.withPackages (p: with p; [
    pulp
    argparse
  ]);

in
pkgs.mkShell {
  packages = [
    R-with-my-packages
    python-with-my-packages
  ];
}